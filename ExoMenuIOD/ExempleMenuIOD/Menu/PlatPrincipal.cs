namespace Menu
{
    internal class PlatPrincipal : IPlat
    {
        public readonly string Nom;

        public PlatPrincipal(string nom)
        {
            Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange le plat principal " + Nom + "\n";
        }

    }
}