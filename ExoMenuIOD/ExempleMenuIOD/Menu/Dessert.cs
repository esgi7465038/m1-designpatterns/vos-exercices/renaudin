namespace Menu
{
    internal class Dessert : IPlat
    {
        public readonly string Nom;

        public Dessert(string nom)
        {
            Nom = nom;
        }

        public string Manger()
        {
            return "=> Je mange le plat principal " + Nom + "\n";
        }

    }
}