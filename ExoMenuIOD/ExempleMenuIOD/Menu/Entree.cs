﻿namespace Menu
{
    internal class Entree : IPlat
    {
        public readonly string Nom;

        public Entree(string nom) 
        { 
            Nom = nom;
        }

        public string Manger() 
        {
            return "=> Je mange l'entrée " + Nom + "\n";
        }

    }
}
