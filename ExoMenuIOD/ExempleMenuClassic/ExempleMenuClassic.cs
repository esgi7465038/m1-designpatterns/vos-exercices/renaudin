﻿Console.WriteLine("************************************");
Console.WriteLine("* Menu classique avec composition. *");
Console.WriteLine("************************************");

Menu.Menu leMenu = new Menu.Menu(1, "Tomate-mozza", "Côte de boeuf", "Mousse au chocolat");

Console.WriteLine(leMenu.Consommer());

