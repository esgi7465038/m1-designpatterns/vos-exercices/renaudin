﻿using Observateur;

namespace ExoObserverConsoleSingleton
{
    internal class SujetConcret : ISujet
    {
        private static SujetConcret? instance = null;
        public string UnMessage => "JE SUIS UNIQUE! La preuve avec mon HashCode: " + this.GetHashCode().ToString();
        public static SujetConcret Instance
        {
            get
            {
                lock (typeof(SujetConcret))
                {
                    if (instance == null)
                    {
                        Console.WriteLine("Création de l'instance du sujet concret");
                        instance = new SujetConcret();
                    }
                    else
                    {
                        Console.WriteLine("L'instance du sujet concret existe déjà");
                    }
                        
                    return instance;
                }
            }
        }

        #region Gestion des observateurs
        /// <summary>
        /// La liste des observateurs.
        /// </summary>
        private readonly List<IObservateur> observateurs = new List<IObservateur>();

        /// <summary>
        /// Le nombre d'observateur inscrits
        /// </summary>
        public int NbObservateur => this.observateurs.Count;

        public void AjouteObservateur(IObservateur observateur)
        {
            if (!this.observateurs.Contains(observateur))
                this.observateurs.Add(observateur);
        }

        public void RetireObservateur(IObservateur observateur)
        {
            if (observateurs.Contains(observateur))
                this.observateurs.Remove(observateur);
        }

        public void NotifierObservateurs()
        {
            foreach (IObservateur observateur in this.observateurs)
                observateur.Actualiser(this);
        }
        #endregion

        /// <summary>
        /// La valeur qui sera observée par les observateurs
        /// </summary>
        public int ValObservee { get; private set; } = 0;


        /// <summary>
        /// Une consigne donnée que la valeur observée cherchera à rejoindre.
        /// </summary>
        private int consigne = 0;

        public int Consigne
        {
            get => consigne;
            set
            {
                if (value < 0)
                    consigne = 0;
                else if (value > 100)
                    consigne = 100;
                else
                    consigne = value;
            }
        }

        private readonly Thread LaTache;
        public bool Stop { get; set; }

        private SujetConcret()
        {
            ValObservee = 0;
            LaTache = new Thread(Simulation);
            Console.WriteLine("Constructeur : " + UnMessage);
            LaTache.Start();
        }

        private void Simulation()
        {
            while (!Stop)
            {
                if (ValObservee != consigne)
                {
                    //  Mise à jour de la valeur observée
                    if (ValObservee < consigne)
                        //ValObservee++;
                        ValObservee += 2; // Modification pour accélérer la simulation
                    else
                        ValObservee--;

                    //  On prévient tous les observateurs
                    this.NotifierObservateurs();
                }
                Thread.Sleep(1000);
            }
        }

        public void Join()
        {
            LaTache.Join(); 
        }
    }
}
