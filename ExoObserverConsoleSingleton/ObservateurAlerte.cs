﻿using Observateur;

namespace ExoObserverConsoleSingleton
{
    internal class ObservateurAlerte : IObservateur
    {
        #region Implémentation de IObservateur
        public void Actualiser(ISujet sujet)
        {
            SujetConcret sujetConcret = (SujetConcret)sujet;
            if (sujetConcret != null)
            {
                int valeur = sujetConcret.ValObservee;
                ConsoleColor oldForeground = Console.ForegroundColor;
                ConsoleColor oldBackground = Console.BackgroundColor;

                if (valeur > 90)
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Alerte !! : Valeur observée: " + valeur);
                }
                else if (valeur > 75)
                {
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine("Attention !! : Valeur observée: " + valeur);
                }
                                
                // Réinitialisation des couleurs de la console à leurs valeurs précédentes
                Console.ForegroundColor = oldForeground;
                Console.WriteLine("--> " + GetHashCode() + " Val obs: " + ((SujetConcret)sujet).ValObservee);
                Console.BackgroundColor = oldBackground;
            }
        }
        #endregion
        private ConsoleColor ForegroundColor { get; set; }

        public ObservateurAlerte(ConsoleColor foregroundColor)
        {
            ForegroundColor = foregroundColor;
        }

    }
}
