﻿using System;
using System.Threading;
using ExoObserverConsoleSingleton;

class Program
{
    private bool stopThread = false;
    static void Main(string[] args)
    {
        Program prog = new Program();
        // Lancement des threads pour tester le Singleton
        Thread thread1 = new (prog.Run_1);
        Thread thread2 = new(prog.Run_2);

        thread1.Start();
        thread2.Start();

        thread1.Join();
        thread2.Join();

        

        // Accès à l'instance du sujet concret
        SujetConcret sujet = SujetConcret.Instance;
        Console.WriteLine($"HashCode du sujet concret : {sujet.GetHashCode()}");

        // Accès à l'instance du sujet concret une deuxième fois pour vérifier le singleton
        SujetConcret deuxiemeInstance = SujetConcret.Instance;
        Console.WriteLine($"HashCode de la deuxième instance : {deuxiemeInstance.GetHashCode()}");

        if (sujet == deuxiemeInstance)
        {
            Console.WriteLine("Les deux instances sont identiques, le modèle Singleton fonctionne comme prévu.");
        }
        else
        {
            Console.WriteLine("Les deux instances sont différentes, ce qui ne devrait pas arriver avec un modèle Singleton.");
        }

        
    }

    public void Run_1()
    {
        // Instanciation des observateurs
        ObservateurConcret obs1 = new(ConsoleColor.Green);
        ObservateurConcret obs2 = new(ConsoleColor.Red);
        ObservateurAlerte obs3 = new(ConsoleColor.Blue);

        SujetConcret sujet = SujetConcret.Instance;
        Thread.Sleep(200);
        Console.WriteLine($"Accès au Singleton depuis le thread {Thread.CurrentThread.ManagedThreadId}, HashCode : {sujet.GetHashCode()}");

        // Modification consigne du sujet
        sujet.Consigne = 30;
        Thread.Sleep(3000);

        // Inscription des observateurs
        Console.WriteLine("Inscription obs1: ");
        sujet.AjouteObservateur(obs1);
        Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
        Thread.Sleep(5000);

        Console.WriteLine("Inscription obs2: ");
        sujet.AjouteObservateur(obs2);
        Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
        Thread.Sleep(5000);

        Console.WriteLine("Inscription obs3: ");
        sujet.AjouteObservateur(obs3);
        Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
        Thread.Sleep(5000);

        // Modification consigne du sujet
        sujet.Consigne = 100;
        Thread.Sleep(10000);

        // Retrait d'un observateur
        Console.WriteLine("Retrait obs1: ");
        sujet.RetireObservateur(obs1);
        Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
        Thread.Sleep(50000); // Ajustement du temps d'attente si nécessaire

        // Arrêt du sujet
        sujet.Stop = true;
        sujet.Join();

        Console.WriteLine("Sujet arrêté");
        sujet.RetireObservateur(obs2);
        sujet.RetireObservateur(obs3);
    }

    public void Run_2()
    {
        // Instanciation des observateurs
        ObservateurConcret obs1_bis = new(ConsoleColor.Green);
        ObservateurConcret obs2_bis = new(ConsoleColor.Red);
        ObservateurAlerte obs3_bis = new(ConsoleColor.Blue);


        SujetConcret sujet_bis = SujetConcret.Instance;
        Thread.Sleep(200);
        Console.WriteLine($"Accès au Singleton depuis le thread {Thread.CurrentThread.ManagedThreadId}, HashCode : {sujet_bis.GetHashCode()}");
        // Modification consigne du sujet
        sujet_bis.Consigne = 90;
        Thread.Sleep(3000);
        // Inscription des observateurs
        Console.WriteLine("Inscription obs1_bis: ");
        sujet_bis.AjouteObservateur(obs1_bis);
        Console.WriteLine(sujet_bis.NbObservateur + " observateur(s) du sujet.");
        Thread.Sleep(5000);

        Console.WriteLine("Inscription obs2_bis: ");
        sujet_bis.AjouteObservateur(obs2_bis);
        Console.WriteLine(sujet_bis.NbObservateur + " observateur(s) du sujet.");
        Thread.Sleep(5000);

        Console.WriteLine("Inscription obs3_bis: ");
        sujet_bis.AjouteObservateur(obs3_bis);
        Console.WriteLine(sujet_bis.NbObservateur + " observateur(s) du sujet.");
        Thread.Sleep(5000);

        // Modification consigne du sujet
        sujet_bis.Consigne = 100;
        Thread.Sleep(10000);

        // Retrait d'un observateur
        Console.WriteLine("Retrait obs1_bis: ");
        sujet_bis.RetireObservateur(obs1_bis);
        Console.WriteLine(sujet_bis.NbObservateur + " observateur(s) du sujet.");
        Thread.Sleep(50000); // Ajustement du temps d'attente si nécessaire

        // Arrêt du sujet
        sujet_bis.Stop = true;
        sujet_bis.Join();

        Console.WriteLine("sujet_bis arrêté");
        sujet_bis.RetireObservateur(obs2_bis);
        sujet_bis.RetireObservateur(obs3_bis);
    }
}
