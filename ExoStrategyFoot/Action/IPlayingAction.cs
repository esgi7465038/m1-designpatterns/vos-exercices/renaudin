﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoStrategyFoot.Action
{
    internal interface IPlayingAction
    {
        public void ActionToDo();
    }
}
