﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExoStrategyFoot.Joueur
{
    internal class Defenseur : PlayerAbstract
    {
        public Defenseur(int numero) : base(numero)
        {
        }

        public override void DoAction()
        {
            PlayingAction?.ActionToDo();
        }
    }
}
