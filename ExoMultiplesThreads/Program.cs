using System;
using System.Threading;
using System.Timers;

namespace ExoMultiplesThreads
{
    class Program
    {
        private static System.Timers.Timer timer;
        private static MyThreadLetter myThreadLetter;

        static void Main(string[] args)
        {
            myThreadLetter = new MyThreadLetter();
            myThreadLetter.Start();
            Console.WriteLine("Thread d�marr� !");

            timer = new System.Timers.Timer(1000);
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Enabled = true;
            Console.WriteLine("Main thread attends 10 secondes...");
            Thread.Sleep(10000);
            myThreadLetter.Stop = true;
            myThreadLetter.Join();
            Console.WriteLine("Main thread termin� !");
            timer.Stop();
            timer.Dispose();
            Console.WriteLine("Appuyer sur une toucher pour quitter...");
            Console.Readline();
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Timer: {0:HH:mm:ss.fff}", e.SignalTime);
        }
    }
}