﻿using ExoObserverConsole;

//  Instanciation des observateurs
ObservateurConcret obs1 = new(ConsoleColor.Green);
ObservateurConcret obs2 = new(ConsoleColor.Red);
ObservateurAlerte obs3 = new(ConsoleColor.Blue);

Console.WriteLine("Instanciation du sujet concret");
SujetConcret sujet = new();

Console.WriteLine("Modification consigne du sujet");
sujet.Consigne = 30; 
Thread.Sleep(3000);

Console.WriteLine("Inscription obs1: ");
sujet.AjouteObservateur(obs1);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(5000);

Console.WriteLine("Inscription obs2: ");
sujet.AjouteObservateur(obs2);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(5000);

Console.WriteLine("Inscription obs3: ");
sujet.AjouteObservateur(obs3);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(5000);

Console.WriteLine("Modification consigne du sujet");
sujet.Consigne = 100;
Thread.Sleep(10000);

Console.WriteLine("Retrait  obs1: ");
sujet.RetireObservateur(obs1);
Console.WriteLine(sujet.NbObservateur + " observateur(s) du sujet.");
Thread.Sleep(50000);

sujet.Stop = true;
sujet.Join();

Console.WriteLine("Sujet arrété");
sujet.RetireObservateur(obs2);

Console.WriteLine("Sujet arrété");
sujet.Join();
sujet.RetireObservateur(obs3);
