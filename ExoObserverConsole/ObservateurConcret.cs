﻿using Observateur;

namespace ExoObserverConsole
{
    internal class ObservateurConcret : IObservateur
    {
        #region Implémentation de IObservateur
        public void Actualiser(ISujet sujet)
        {
            ConsoleColor old = Console.ForegroundColor;
            Console.ForegroundColor = ForegroundColor;
            Console.WriteLine("--> " + GetHashCode() + " Val obs: " + ((SujetConcret)sujet).ValObservee);
            Console.ForegroundColor = old;
        }
        #endregion

        private ConsoleColor ForegroundColor { get; set; }

        public ObservateurConcret(ConsoleColor foregroundColor)
        {
            ForegroundColor = foregroundColor;
        }

    }
}
