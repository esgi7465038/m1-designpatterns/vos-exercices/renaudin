namespace Vannes
{
    internal class VanneRegulee : Vanne, IRegulable
    {
        public bool isReguleOn { get; set; }

        public void Regular(int consigneDebit)
        {
            if (consigneDebit < debitMax)
            {
                Position = (consigneDebit <= debitMax ? consigneDebit * 100 / debitMax;
            }
        }

        public VanneRegulee(bool isReguleOn = true)
        {
            this.isReguleOn = isReguleOn;
        }
    }
}
