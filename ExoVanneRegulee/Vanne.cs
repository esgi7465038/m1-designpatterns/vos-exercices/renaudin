﻿namespace Vannes
{
    internal class Vanne
    {
        private int position = 0;
        public readonly int debitMax;
        private int debit;

        public int Position { get => position; set => position = (value >= 0 && value <= 100)? value : position; }
        public int Debit { get => debit = debitMax*position/100; }
        public bool EstFermee { get; private set; }

        public Vanne(int debitMax = 100) 
        {
            this.debitMax = debitMax;
            this.Ouvrir(0);
        }

        public void Ouvrir(int ouverture)
        {
            Position = ouverture;
        }

        public override string ToString()
        {
            string msg = "Vanne: " + this.GetHashCode() + ", débit: " + this.debit + ", position: " + this.position;
            return msg;
        }
    }
}
