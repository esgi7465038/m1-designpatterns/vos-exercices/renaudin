﻿// See https://aka.ms/new-console-template for more information

using Vannes;

Vanne? MaVanne;			    // Déclaration de l'objet MaVanne (? => nullable...)
MaVanne = new Vanne(200);	// Instanciation de l'objet MaVanne

VanneRegulee? MaVanne2;
MaVanne2 = new VanneRegulee(); // Instanciation de l'objet MaVanne2

Console.Write("Ouvrir MaVanne: ");
MaVanne.Position = Convert.ToInt32(Console.ReadLine()); 	        // affectation avec set
Console.WriteLine("MaVanne est ouverte à {0}", MaVanne.Position);   // lecture avec get
Console.WriteLine("\nDescription de MaVanne: {0}", MaVanne);        //  Utilisation de ToString

if (MaVanne.Position > 75)
    MaVanne.Ouvrir(75);
Console.WriteLine("Débit: " + MaVanne.Debit);

Console.Write("Ouvrir MaVanne2: ");
MaVanne2.Position = Convert.ToInt32(Console.ReadLine()); 	        // affectation avec set
Console.WriteLine("MaVanne2 est ouverte à {0}", MaVanne2.Position); // lecture avec get
Console.WriteLine("\nDescription de MaVanne2: {0}", MaVanne2);      //  Utilisation de ToString
MaVanne2.Regular(50);
Console.WriteLine("Débit: " + MaVanne2.Debit);

