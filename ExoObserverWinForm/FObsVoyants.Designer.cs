﻿namespace ExoObserverWinForm
{
	partial class FObsVoyants
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            bClose = new Button();
            VoyantVert = new PictureBox();
            VoyantOrange = new PictureBox();
            ((System.ComponentModel.ISupportInitialize)VoyantVert).BeginInit();
            ((System.ComponentModel.ISupportInitialize)VoyantOrange).BeginInit();
            SuspendLayout();
            // 
            // bClose
            // 
            bClose.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            bClose.Location = new Point(89, 91);
            bClose.Margin = new Padding(4, 3, 4, 3);
            bClose.Name = "bClose";
            bClose.Size = new Size(88, 27);
            bClose.TabIndex = 0;
            bClose.Text = "Fermer";
            bClose.UseVisualStyleBackColor = true;
            bClose.Click += BFermer_Click;
            // 
            // VoyantVert
            // 
            VoyantVert.BackColor = Color.LimeGreen;
            VoyantVert.Location = new Point(27, 14);
            VoyantVert.Margin = new Padding(4, 3, 4, 3);
            VoyantVert.Name = "VoyantVert";
            VoyantVert.Size = new Size(51, 48);
            VoyantVert.TabIndex = 2;
            VoyantVert.TabStop = false;
            VoyantVert.Visible = false;
            // 
            // VoyantOrange
            // 
            VoyantOrange.BackColor = Color.Orange;
            VoyantOrange.Location = new Point(124, 14);
            VoyantOrange.Margin = new Padding(4, 3, 4, 3);
            VoyantOrange.Name = "VoyantOrange";
            VoyantOrange.Size = new Size(51, 48);
            VoyantOrange.TabIndex = 3;
            VoyantOrange.TabStop = false;
            VoyantOrange.Visible = false;
            // 
            // FObsVoyants
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(190, 132);
            ControlBox = false;
            Controls.Add(VoyantOrange);
            Controls.Add(VoyantVert);
            Controls.Add(bClose);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Margin = new Padding(4, 3, 4, 3);
            MinimumSize = new Size(206, 171);
            Name = "FObsVoyants";
            Text = "Obs. Voyants";
            FormClosing += FObsVoyants_FormClosing;
            ((System.ComponentModel.ISupportInitialize)VoyantVert).EndInit();
            ((System.ComponentModel.ISupportInitialize)VoyantOrange).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Button bClose;
		private System.Windows.Forms.PictureBox VoyantVert;
		private System.Windows.Forms.PictureBox VoyantOrange;
	}
}