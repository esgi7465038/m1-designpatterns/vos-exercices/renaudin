﻿using Observateur;

namespace ExoObserverWinForm
{
    public partial class FObsLogger : Form, IObservateur
	{
        private readonly SujetConcret Sujet;

		public FObsLogger(SujetAbstrait sujet)
		{
			InitializeComponent();
			Sujet = (SujetConcret)sujet;
			Sujet.AjouteObservateur(this);
			Actualiser(Sujet);
		}

		public void Actualiser(ISujet sujet)
		{
			tBValObservees.AppendText(Sujet.ValObservee.ToString()+"\r\n");
		}

        private void BClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void FObsLogger_FormClosing(object sender, FormClosingEventArgs e)
		{
			Sujet.RetireObservateur(this);
		}
	}
}
