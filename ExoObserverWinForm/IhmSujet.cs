﻿namespace ExoObserverWinForm
{
    public partial class IhmSujet : Form
    {
        //  Le sujet concret...
        private readonly SujetConcret Sujet;

        public IhmSujet()
        {
            InitializeComponent();

            //  Création et mise à jour du sujet concret 
            Sujet = new SujetConcret();
            Sujet.Consigne = this.hSbConsigne.Value;
            this.lValeurObservee.Text = Sujet.ValObservee.ToString();

            tMaJIhm.Start();
        }

        //  Gestionnaire d'évènement du timer de mise à jour de l'ihm.
        private void TMaJIhm_Tick(object sender, EventArgs e)
        {
            this.lValeurObservee.Text = Sujet.ValObservee.ToString();
            this.lNbObs.Text = Sujet.NbObservateur.ToString();
            this.hSbConsigne.Value = Sujet.Consigne;
        }

        //  Gestionnaire d'évènement de changement de la consigne
        private void HSbConsigne_ValueChanged(object sender, EventArgs e)
        {
            Sujet.Consigne = this.hSbConsigne.Value;
            this.lConsigne.Text = this.hSbConsigne.Value.ToString();
        }

        //  Les 3 gestionnaires d'évènements des boutons
        // de création des observateurs.
        private void BNewObsAff_Click(object sender, EventArgs e)
        {
            FObsAfficheur Obs = new FObsAfficheur(Sujet);
            Obs.Show();
        }

        private void BNewObsLogger_Click(object sender, EventArgs e)
        {
            FObsLogger Obs = new FObsLogger(Sujet);
            Obs.Show();
        }

        private void BNewObsVoyant_Click(object sender, EventArgs e)
        {
            FObsVoyants Obs = new FObsVoyants(Sujet);
            Obs.Show();
        }
    }
}
