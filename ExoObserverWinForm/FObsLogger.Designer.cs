﻿namespace ExoObserverWinForm
{
	partial class FObsLogger
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tBValObservees = new TextBox();
            bClose = new Button();
            SuspendLayout();
            // 
            // tBValObservees
            // 
            tBValObservees.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            tBValObservees.Location = new Point(8, 12);
            tBValObservees.Margin = new Padding(4, 3, 4, 3);
            tBValObservees.Multiline = true;
            tBValObservees.Name = "tBValObservees";
            tBValObservees.ReadOnly = true;
            tBValObservees.ScrollBars = ScrollBars.Vertical;
            tBValObservees.Size = new Size(101, 280);
            tBValObservees.TabIndex = 0;
            // 
            // bClose
            // 
            bClose.Location = new Point(145, 256);
            bClose.Margin = new Padding(4, 3, 4, 3);
            bClose.Name = "bClose";
            bClose.Size = new Size(88, 27);
            bClose.TabIndex = 1;
            bClose.Text = "Fermer";
            bClose.UseVisualStyleBackColor = true;
            bClose.Click += BClose_Click;
            // 
            // FObsLogger
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(246, 297);
            ControlBox = false;
            Controls.Add(bClose);
            Controls.Add(tBValObservees);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Margin = new Padding(4, 3, 4, 3);
            MinimumSize = new Size(262, 336);
            Name = "FObsLogger";
            Text = "Obs. Logger";
            FormClosing += FObsLogger_FormClosing;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.TextBox tBValObservees;
		private System.Windows.Forms.Button bClose;
	}
}