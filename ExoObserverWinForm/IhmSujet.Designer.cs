﻿namespace ExoObserverWinForm
{
	partial class IhmSujet
	{
		/// <summary>
		/// Variable nécessaire au concepteur.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Nettoyage des ressources utilisées.
		/// </summary>
		/// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Code généré par le Concepteur Windows Form

		/// <summary>
		/// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette méthode avec l'éditeur de code.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.lNbObs = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lValeurObservee = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.lConsigne = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.hSbConsigne = new System.Windows.Forms.HScrollBar();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.bNewObsLogger = new System.Windows.Forms.Button();
			this.bNewObsVoyant = new System.Windows.Forms.Button();
			this.bNewObsAff = new System.Windows.Forms.Button();
			this.tMaJIhm = new System.Windows.Forms.Timer(this.components);
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.lNbObs);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.lValeurObservee);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.lConsigne);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.hSbConsigne);
			this.groupBox1.Location = new System.Drawing.Point(4, 86);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(327, 97);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Le sujet concret";
			// 
			// lNbObs
			// 
			this.lNbObs.AutoSize = true;
			this.lNbObs.Location = new System.Drawing.Point(283, 24);
			this.lNbObs.Name = "lNbObs";
			this.lNbObs.Size = new System.Drawing.Size(13, 13);
			this.lNbObs.TabIndex = 6;
			this.lNbObs.Text = "0";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(186, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(91, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Nb d\'observateur:";
			// 
			// lValeurObservee
			// 
			this.lValeurObservee.AutoSize = true;
			this.lValeurObservee.Location = new System.Drawing.Point(104, 24);
			this.lValeurObservee.Name = "lValeurObservee";
			this.lValeurObservee.Size = new System.Drawing.Size(13, 13);
			this.lValeurObservee.TabIndex = 4;
			this.lValeurObservee.Text = "0";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(11, 24);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(87, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Valeur observée:";
			// 
			// lConsigne
			// 
			this.lConsigne.AutoSize = true;
			this.lConsigne.Location = new System.Drawing.Point(193, 77);
			this.lConsigne.Name = "lConsigne";
			this.lConsigne.Size = new System.Drawing.Size(13, 13);
			this.lConsigne.TabIndex = 2;
			this.lConsigne.Text = "0";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(133, 77);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(54, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Consigne:";
			// 
			// hSbConsigne
			// 
			this.hSbConsigne.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.hSbConsigne.LargeChange = 5;
			this.hSbConsigne.Location = new System.Drawing.Point(5, 52);
			this.hSbConsigne.Name = "hSbConsigne";
			this.hSbConsigne.Size = new System.Drawing.Size(317, 18);
			this.hSbConsigne.TabIndex = 0;
			this.hSbConsigne.ValueChanged += new System.EventHandler(this.HSbConsigne_ValueChanged);
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.bNewObsLogger);
			this.groupBox2.Controls.Add(this.bNewObsVoyant);
			this.groupBox2.Controls.Add(this.bNewObsAff);
			this.groupBox2.Location = new System.Drawing.Point(5, 8);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(326, 78);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Création des observateurs";
			// 
			// bNewObsLogger
			// 
			this.bNewObsLogger.Location = new System.Drawing.Point(221, 19);
			this.bNewObsLogger.Name = "bNewObsLogger";
			this.bNewObsLogger.Size = new System.Drawing.Size(94, 53);
			this.bNewObsLogger.TabIndex = 2;
			this.bNewObsLogger.Text = "Logger";
			this.bNewObsLogger.UseVisualStyleBackColor = true;
			this.bNewObsLogger.Click += new System.EventHandler(this.BNewObsLogger_Click);
			// 
			// bNewObsVoyant
			// 
			this.bNewObsVoyant.Location = new System.Drawing.Point(114, 19);
			this.bNewObsVoyant.Name = "bNewObsVoyant";
			this.bNewObsVoyant.Size = new System.Drawing.Size(94, 53);
			this.bNewObsVoyant.TabIndex = 1;
			this.bNewObsVoyant.Text = "Voyant";
			this.bNewObsVoyant.UseVisualStyleBackColor = true;
			this.bNewObsVoyant.Click += new System.EventHandler(this.BNewObsVoyant_Click);
			// 
			// bNewObsAff
			// 
			this.bNewObsAff.Location = new System.Drawing.Point(7, 19);
			this.bNewObsAff.Name = "bNewObsAff";
			this.bNewObsAff.Size = new System.Drawing.Size(94, 53);
			this.bNewObsAff.TabIndex = 0;
			this.bNewObsAff.Text = "Afficheur";
			this.bNewObsAff.UseVisualStyleBackColor = true;
			this.bNewObsAff.Click += new System.EventHandler(this.BNewObsAff_Click);
			// 
			// tMaJIhm
			// 
			this.tMaJIhm.Tick += new System.EventHandler(this.TMaJIhm_Tick);
			// 
			// IhmSujet
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(335, 185);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "IhmSujet";
			this.Text = "Démo du DP \"Observer\"";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.HScrollBar hSbConsigne;
		private System.Windows.Forms.Label lNbObs;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lValeurObservee;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lConsigne;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button bNewObsAff;
		private System.Windows.Forms.Button bNewObsLogger;
		private System.Windows.Forms.Button bNewObsVoyant;
		private System.Windows.Forms.Timer tMaJIhm;
	}
}

