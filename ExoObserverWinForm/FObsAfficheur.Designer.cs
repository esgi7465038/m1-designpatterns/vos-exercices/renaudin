﻿namespace ExoObserverWinForm
{
	partial class FObsAfficheur
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lValObservee = new Label();
            bClose = new Button();
            SuspendLayout();
            // 
            // lValObservee
            // 
            lValObservee.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            lValObservee.AutoSize = true;
            lValObservee.Font = new Font("Microsoft Sans Serif", 20F, FontStyle.Bold, GraphicsUnit.Point, 0);
            lValObservee.Location = new Point(117, 10);
            lValObservee.Margin = new Padding(4, 0, 4, 0);
            lValObservee.Name = "lValObservee";
            lValObservee.Size = new Size(30, 31);
            lValObservee.TabIndex = 0;
            lValObservee.Text = "0";
            // 
            // bClose
            // 
            bClose.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            bClose.Location = new Point(172, 68);
            bClose.Margin = new Padding(4, 3, 4, 3);
            bClose.Name = "bClose";
            bClose.Size = new Size(88, 27);
            bClose.TabIndex = 1;
            bClose.Text = "Fermer";
            bClose.UseVisualStyleBackColor = true;
            bClose.MouseClick += BClose_MouseClick;
            // 
            // FObsAfficheur
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(273, 109);
            ControlBox = false;
            Controls.Add(bClose);
            Controls.Add(lValObservee);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Margin = new Padding(4, 3, 4, 3);
            MinimumSize = new Size(289, 148);
            Name = "FObsAfficheur";
            Text = "Obs. Afficheur";
            FormClosing += FObsAfficheur_FormClosing;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label lValObservee;
		private System.Windows.Forms.Button bClose;
	}
}