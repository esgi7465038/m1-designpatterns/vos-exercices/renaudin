﻿using Observateur;

namespace ExoObserverWinForm
{
    //  Le sujet concret hérite du sujet abstrait
    internal class SujetConcret : SujetAbstrait
    {
        //  La valeur qui sera observée par les observateurs
        public int ValObservee { get; private set; } = 0;

        //  Une consigne que la valeur observée cherchera à rejoindre.
        private int consigne;
        public int Consigne
        {
            get => consigne;
            set
            {
                if (value < 0)
                    consigne = 0;
                else if (value > 100)
                    consigne = 100;
                else
                    consigne = value;
            }
        }

        //  Le timer utilisé pour faire varier la donnée observée.
        private System.Windows.Forms.Timer hlg;
        public const int intervaleHlg = 555;    // en ms

        public SujetConcret()
        {
            //  Initialisation des attributs.
            ValObservee = 0;
            consigne = 0;

            //  Création, initialisation et démmarage du timer.
            hlg = new System.Windows.Forms.Timer();
            hlg.Interval = intervaleHlg;
            hlg.Tick += new EventHandler(Simulation);
            hlg.Start();
        }

        //  Le gestionnaire d'événement du timer.
        void Simulation(object? sender, EventArgs e)
        {
            if (ValObservee != consigne)
            {
                //  Mise à jour de la valeur observée
                if (ValObservee < consigne)
                    ValObservee++;
                else
                    ValObservee--;

                //  On prévient tous les observateurs
                NotifierObservateurs();
            }
        }
    }
}
