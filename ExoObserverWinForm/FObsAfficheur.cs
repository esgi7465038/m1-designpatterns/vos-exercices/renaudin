﻿using Observateur;

namespace ExoObserverWinForm
{
    //  Hérite de Form et implémente IObservateur.
    public partial class FObsAfficheur : Form, IObservateur
    {
        //  Le sujet concret...
        private readonly SujetConcret Sujet;

        //  L'association est réalisée dès la construction.
        public FObsAfficheur(SujetAbstrait sujet)
        {
            InitializeComponent();

            //  Réalisation de l'association.
            Sujet = (SujetConcret)sujet;

            //  Inscription dans la liste des observateurs du sujet concret.
            Sujet.AjouteObservateur(this);

            //  L'observateur se met à jour selon l'état du sujet.
            Actualiser(Sujet);
        }

        public void Actualiser(ISujet sujet)
        {
            //  Exécuté lors de chaque notification de changement
            //  signalée par le sujet concret.
            lValObservee.Text = Sujet.ValObservee.ToString();
        }

        //  Gestionaire d'événement lors d'une demande
        //  de fermeture de la fenètre.
        private void FObsAfficheur_FormClosing(object sender, FormClosingEventArgs e)
        {
            //  L'observateur ne doit pas oublier de se désabonner
            //  du sujet concret lorsqu'il est détruit.
            Sujet.RetireObservateur(this);
        }

        private void BClose_MouseClick(object sender, MouseEventArgs e)
        {
            this.Close();
        }
    }
}
