﻿using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace ExoObserverWinForm
{
    partial class FObsNewVoyant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            bClose = new Button();
            VoyantVert = new PictureBox();
            VoyantOrange = new PictureBox();
            ((System.ComponentModel.ISupportInitialize)VoyantVert).BeginInit();
            ((System.ComponentModel.ISupportInitialize)VoyantOrange).BeginInit();
            SuspendLayout();
            // 
            // FObsNewVoyant
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Name = "FObsNewVoyant";
            Text = "FObsNewVoyant";
            Load += FObsNewVoyant_Load;
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.Button bClose;
        private System.Windows.Forms.PictureBox VoyantVert;
        private System.Windows.Forms.PictureBox VoyantOrange;
    }
}