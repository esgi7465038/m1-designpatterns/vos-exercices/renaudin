﻿using Observateur;

namespace ExoObserverWinForm
{
    //  Hérite de Form et implémente IObservateur.
    public partial class FObsVoyants : Form, IObservateur
{
	//  Le sujet concret...
	private readonly SujetConcret Sujet;

	//  L'association est réalisée dès la construction.
	public FObsVoyants(SujetAbstrait sujet)
	{
		InitializeComponent();

		//  Réalisation de l'association.
		Sujet = (SujetConcret)sujet;

		//  Inscription dans la liste des observateurs du sujet concret.
		Sujet.AjouteObservateur(this);

		//  L'observateur se met à jour selon l'état du sujet.
		Actualiser(Sujet);
	}

	public void Actualiser(ISujet sujet)
	{
		//  Exécuté lors de chaque notification de changement
		//  signalée par le sujet concret.
		int valObservee = Sujet.ValObservee;

		//  La logique d'allumage des voyants
		if (valObservee < 5)
		{
			VoyantVert.Hide();
			VoyantOrange.Hide();
		}
		else if (valObservee < 70)
		{
			VoyantVert.Show();
			VoyantOrange.Hide();
		}
		else if (valObservee < 75)
		{
			VoyantVert.Show();
			VoyantOrange.Show();
		}
		else if (valObservee < 80)
		{
			VoyantVert.Hide();
			VoyantOrange.Show();
		}
		else if (valObservee > 85)
		{
			//  Rien n'interdit à l'observateur
			//  d'agir sur le sujet...
			Sujet.Consigne = 69;
		}
	}

	//  Gestionaire d'événement lors d'une demande
	//  de fermeture de la fenètre.
	private void FObsVoyants_FormClosing(object sender, FormClosingEventArgs e)
	{
		//  L'observateur ne doit pas oublier de se désabonner
		//  du sujet concret lorsqu'il est détruit.
		Sujet.RetireObservateur(this);
	}

	private void BFermer_Click(object sender, EventArgs e)
	{
		this.Close();
	}
}
}
