﻿namespace Feutres
{
    internal class Feutre
    {
        /// Un attribut et sa property en lecture seulement
        private string matiere;
        public string Matiere => matiere;

        /// Un attribut et sa property en lecture et écriture
        private ConsoleColor couleur;
        public ConsoleColor Couleur
        {
            get => couleur;
            set => couleur = value;
        }

        /// Auto-property, on peut parfois se passer d'attribut.
        /// Le mutateur (set) peut aussi avoir une visibilité plus restrictive (ici private)
        public bool EstBouche { get; private set; }

        public Feutre()
        {
            // Initialisation des attributs 
            // avec des valeurs par défaut.
            this.matiere = "Métal";
            this.Couleur = ConsoleColor.Blue;
            this.EstBouche = true;
        }

        /// Constructeur avec des paramètres 
        /// 	pour initialiser les attributs
        public Feutre(string matiere,
                     ConsoleColor couleur,
                     bool estBouche = true)
        {
            this.matiere = matiere;
            this.Couleur = couleur;
            this.EstBouche = estBouche;
        }
        public bool Debouche()
        {
            EstBouche = false;
            return EstBouche;
        }

        public bool Bouche()
        {
            EstBouche = true;
            return EstBouche;
        }

        public bool Ecrire(string message)
        {
            if (!EstBouche)
            {
                ConsoleColor couleurActuelle = Console.ForegroundColor;
                Console.ForegroundColor = couleur;
                Console.WriteLine(message);
                Console.ForegroundColor = couleurActuelle;
                return true;
            } else
            {
                Console.WriteLine("Le feutre est bouché !");
                return false;
            }
        }
    }
}
