﻿using Menu;

namespace Factory
{
    internal abstract class PlatFactoryAbstract
    {
        public abstract IPlat CreateEntree(string nom);
        public abstract IPlat CreatePlatPrincipal(string nom);
        public abstract IPlat CreateDessert(string nom);
    }
}
