﻿using Factory;
using Menu;

namespace ExoFactory.Factory
{
    internal class PlatEnfantFactory : PlatFactoryAbstract
    {
        public override IPlat CreateEntree(string nom)
        {
            return new EntreeEnfant(nom);
        }

        public override IPlat CreatePlatPrincipal(string nom)
        {
            return new PlatPrincipalEnfant(nom);
        }

        public override IPlat CreateDessert(string nom)
        {
            return new DessertEnfant(nom);
        }
    }
}
