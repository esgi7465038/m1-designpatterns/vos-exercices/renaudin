﻿using Factory;
using Menu;

namespace ExoFactory.Factory
{
    internal class PlatAdulteFactory : PlatFactoryAbstract
    {
        public override IPlat CreateEntree(string nom)
        {
            return new EntreeAdulte(nom);
        }

        public override IPlat CreatePlatPrincipal(string nom)
        {
            return new PlatPrincipalAdulte(nom);
        }

        public override IPlat CreateDessert(string nom)
        {
            return new DessertAdulte(nom);
        }
    }
}
