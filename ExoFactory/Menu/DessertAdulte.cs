﻿using ExoFactory.Menu;

namespace Menu
{
    internal class DessertAdulte : DessertAbstract
    {
        public DessertAdulte(string nom) : base(nom)
        {
        }

        public override string Manger()
        {
            return "=> Je mange le dessert adulte " + Nom + "\n";
        }
    }
}
