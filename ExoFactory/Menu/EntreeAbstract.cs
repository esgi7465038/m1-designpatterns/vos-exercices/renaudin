﻿using Menu;

namespace ExoFactory.Menu
{
    internal abstract class EntreeAbstract : IPlat
    {
        public readonly string Nom;

        public EntreeAbstract(string nom)
        {
            Nom = nom;
        }

        public abstract string Manger();

    }
}
