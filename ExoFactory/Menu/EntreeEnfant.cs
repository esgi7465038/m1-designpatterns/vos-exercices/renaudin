﻿using ExoFactory.Menu;

namespace Menu
{
    internal class EntreeEnfant : EntreeAbstract
    {
        public EntreeEnfant(string nom) : base(nom)
        {
        }

        public override string Manger()
        {
            return "=> Je mange l'entrée enfant " + Nom + "\n";
        }
    }
}
