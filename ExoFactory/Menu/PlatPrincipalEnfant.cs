﻿using ExoFactory.Menu;

namespace Menu
{
    internal class PlatPrincipalEnfant : PlatPrincipalAbstract
    {
        public PlatPrincipalEnfant(string nom) : base(nom)
            {
        }

        public override string Manger()
        {
            return "=> Je mange le plat principal enfant " + Nom + "\n";
        }
    }
}
