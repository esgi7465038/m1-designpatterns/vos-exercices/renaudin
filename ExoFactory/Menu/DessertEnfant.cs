﻿using ExoFactory.Menu;

namespace Menu
{
    internal class DessertEnfant : DessertAbstract
    {
        public DessertEnfant(string nom) : base(nom)
        {
        }

        public override string Manger()
        {
            return "=> Je mange le dessert enfant " + Nom + "\n";
        }
    }
}
