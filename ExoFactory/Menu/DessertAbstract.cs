﻿namespace Menu
{
    internal abstract class DessertAbstract : IPlat
    {
        public readonly string Nom;

        public DessertAbstract(string nom)
        {
            Nom = nom;
        }

        public abstract string Manger();
    }
}
