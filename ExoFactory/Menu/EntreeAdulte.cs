﻿using ExoFactory.Menu;

namespace Menu
{
    internal class EntreeAdulte : EntreeAbstract
    {
        
        public EntreeAdulte(string nom) : base(nom)
        {
        }

        public override string Manger()
        {
            return "=> Je mange l'entrée adulte " + Nom + "\n";
        }
    }
}
