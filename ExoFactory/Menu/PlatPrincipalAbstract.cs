﻿namespace Menu
{
    internal abstract class PlatPrincipalAbstract : IPlat
    {
        public readonly string Nom;

        public PlatPrincipalAbstract(string nom)
        {
            Nom = nom;
        }

        public abstract string Manger();
    }
}
