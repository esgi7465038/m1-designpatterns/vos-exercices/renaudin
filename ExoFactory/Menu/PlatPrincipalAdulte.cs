﻿using ExoFactory.Menu;
namespace Menu
{
    internal class PlatPrincipalAdulte : PlatPrincipalAbstract
    {
        public PlatPrincipalAdulte(string nom) : base(nom)
        {
        }

        public override string Manger()
        {
            return "=> Je mange le plat principal adulte " + Nom + "\n";
        }
    }
}
