﻿using ExoFactory.Factory;
using Factory;
using Menu;

Console.WriteLine("*************************");
Console.WriteLine("* Menu avec PlatFactory *");
Console.WriteLine("*************************");

//  Création de la fabrique
PlatAdulteFactory adulteFactory = new PlatAdulteFactory();
Console.WriteLine("------------ Création de plats pour adulte ------------");
PlatEnfantFactory enfantFactory = new PlatEnfantFactory();
Console.WriteLine("------------ Création de plats pour enfant ------------");

//  Création du menu
Menu.Menu menu1 = new Menu.Menu(1);

//  Création de plats par la factory
IPlat entree = adulteFactory.CreateEntree("Salade verte");
PlatPrincipalAbstract platPrincipal = (PlatPrincipalAbstract)adulteFactory.CreatePlatPrincipal("Dos de cabillaud");

//  Injection des plats au menu et consommation du menu
menu1.AddPlat(entree).AddPlat(platPrincipal).AddPlat(adulteFactory.CreateDessert("Tiramizu"));
Console.WriteLine(menu1.Consommer());
Console.WriteLine();

//  Création d'un tableau de Iplat
IPlat[] plats = { adulteFactory.CreateEntree("Nems"),
    adulteFactory.CreateEntree("Giozza"),
    adulteFactory.CreatePlatPrincipal("Bobun"),
    adulteFactory.CreateDessert("Moshi"),
    adulteFactory.CreateDessert("Nougat")
};

//  Création du menu avec injection des plats par le constructeur et consommation
Menu.Menu menu2 = new Menu.Menu(2, plats);
Console.WriteLine(menu2.Consommer());

IPlat[] platsEnfant = { enfantFactory.CreateEntree("Tomates cerises"),
    enfantFactory.CreateEntree("Mozza"),
    enfantFactory.CreatePlatPrincipal("Steak - Frites"),
    enfantFactory.CreateDessert("Glace"),
    enfantFactory.CreateDessert("Assortiments de bonbons")
};

Menu.Menu menu3 = new Menu.Menu(3, platsEnfant);
Console.WriteLine(menu3.Consommer());

