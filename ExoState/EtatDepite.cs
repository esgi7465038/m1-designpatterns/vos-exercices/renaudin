﻿namespace Etats
{
    class EtatDepite : IEtatEtudiant
    {
        public string DireBonjour(Etudiant contexte)
        {
            return "Pfff Ggrrrrrr";
        }

        public void PartirEnVacance(Etudiant contexte)
        {
            Console.WriteLine("Youpi! J'étais Dépité, je deviens Heureux");
            contexte.EtatCourant = contexte.LesEtats[Etudiant.etatHeureux];
        }

        public void RentrerDeVacance(Etudiant contexte)
        {
            throw new TransitionImpossibleException("Hé Je suis déjà Dépité .. !");
         
        }

        public void Travailler(Etudiant contexte, bool aFaire)
        {
            if (aFaire)
            {
                Console.WriteLine("Pff, Je suis déjà Dépité...");
            }
            else
            {
                Console.WriteLine("Mouais, j'étais Dépité, je deviens Contrarié");
                contexte.EtatCourant = contexte.LesEtats[Etudiant.etatContrarie];
            }
        }
    }
}