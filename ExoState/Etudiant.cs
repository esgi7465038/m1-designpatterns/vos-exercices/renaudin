﻿namespace Etats
{
    class Etudiant
    {
        /// <summary>
        /// Le nom de l'étudiant
        /// </summary>
        public readonly string nom;

        public bool aFaire;

        //  Indices des états concrets dans le tableau des états.
        public const int etatHeureux = 0;
        public const int etatContrarie = 1;
        public const int etatDepite = 2;

        public IEtatEtudiant EtatCourant { get; set; }

        //  Déclaration du tableau des états possibles, attribut et propriété
        private const int nbEtatsPossibles = 3;

        public IEtatEtudiant[] LesEtats { get; }

        public Etudiant(string nom)
        {
            this.nom = nom;

            //  Création des états concrets possibles
            LesEtats = new IEtatEtudiant[nbEtatsPossibles];
            LesEtats[etatHeureux] = new EtatHeureux();
            LesEtats[etatContrarie] = new EtatContrarie();
            LesEtats[etatDepite] = new EtatDepite();

            //  Fixe l'état initial
            EtatCourant = LesEtats[etatHeureux];
        }

        #region Les comportements
        //  Les comportements qui dépendent de l'état et/ou
        //  qui peuvent provoquer une transition sont délégués à l'état courant


        public String DireBonjour()
        {
            //  Les comportements encapsulés dans les états recoivent le contexte,
            //  cad l'objet courant en paramètre.
            return EtatCourant.DireBonjour(this);
        }

        public void PartirEnVacance()
        {
            EtatCourant.PartirEnVacance(this);
        }

        public void RentrerDeVacance()
        {
            EtatCourant.RentrerDeVacance(this);
        }

        public void Travailler(bool aFaire)
        {
            this.aFaire = aFaire;
            EtatCourant.Travailler(this, aFaire);
        }
        #endregion
    }
}
