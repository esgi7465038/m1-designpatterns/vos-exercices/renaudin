﻿namespace Etats
{
    //  Les états concret implémentent l'interface etat abstrait...
    //  et donc tous les comportements dépendant de l'état.
    class EtatHeureux : IEtatEtudiant
    {
        //  Certain comportement ne provoqueront jamais de transition.
        public string DireBonjour(Etudiant contexte)
        {
            return "Helloooo !! I'am happy!";
        }

        //  Certain ne font rien dans cet état concret.
        //  Dans un autre état, ce comportement pourra 
        //  effectuer un traitement et / ou provoquer une transition
        public void PartirEnVacance(Etudiant contexte)
        {
        }

        //  Ce comportement effectue un traitement et provoque une transition
        public void RentrerDeVacance(Etudiant contexte)
        {
            Console.WriteLine("J'étais Heureux... Maintenant je suis Contrarié!");
            contexte.EtatCourant = contexte.LesEtats[Etudiant.etatContrarie];
        }

        public void Travailler(Etudiant contexte, bool aFaire)
        {
            throw new TransitionImpossibleException("Hé c'est encore les vacances ! Un peu plus et c'est les Prud'hommes !");
        }

    }
}
