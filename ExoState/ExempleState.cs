﻿using Etats;

Etudiant UnEtudiant = new Etudiant("Pal");

//  Etat Initial -> Heureux
Console.WriteLine(UnEtudiant.DireBonjour());

UnEtudiant.PartirEnVacance();
//  Pas de transition, reste Heureux
Console.WriteLine(UnEtudiant.DireBonjour());

UnEtudiant.RentrerDeVacance();
//  --> Contrarié
Console.WriteLine(UnEtudiant.DireBonjour());

Console.WriteLine("Travailler avec une tâche déjà faite ?");
UnEtudiant.Travailler(false);
//  --> Dépité car la tâche est à faire.
Console.WriteLine(UnEtudiant.DireBonjour());


Console.WriteLine("Travailler avec une tâche à faire ?");
UnEtudiant.Travailler(true);
//  --> Contrarié
Console.WriteLine(UnEtudiant.DireBonjour());


UnEtudiant.PartirEnVacance();
//  --> Heureux
Console.WriteLine(UnEtudiant.DireBonjour());


try
{
    //  Lance une exception TransitionImpossibleException
    UnEtudiant.Travailler(true);
    Console.WriteLine(UnEtudiant.DireBonjour());
}
catch (TransitionImpossibleException ex)
{
    Console.WriteLine("Exception Transition Impossible: " + ex.Message);
}
finally
{
    Console.WriteLine(UnEtudiant.DireBonjour());
}

