﻿namespace Etats
{
    //  L'interface etat abstrait qui devra être implémentée par
    //  tous les états concrets.
    interface IEtatEtudiant
    {
        string DireBonjour(Etudiant contexte);
        void PartirEnVacance(Etudiant contexte);
        void RentrerDeVacance(Etudiant contexte);
        void Travailler(Etudiant contexte, bool aFaire);
    }
}
